import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'art-store';
  ngOnInit() {

    document.getElementById('sidebarCollapse').addEventListener('click',()=>{
      document.getElementById('sidebar').classList.toggle('active');
    })
     
  }
}
